## My FreeBSD ports tree
This is my custom FreeBSD ports overlay that I use for testing and development.

## How to use this repository as an overlay with Poudriere

1. Install poudriere-devel package:

```
# pkg install poudriere-devel
```

**Note:** `poudriere-devel` != `poudriere`. See [Poudriere](https://github.com/freebsd/poudriere/wiki)


2. Configure ports trees

```
# poudriere ports -c -p default
```

Add this ports tree:

```
# poudriere ports -c -U https://gitlab.com/0xDRRB/drrbFreeBSDports.git -p drrbports -m git -B master
```

3. Make a jail:

```
# poudriere jail -c -j "myjail" -v "13.1-RELEASE"
```

4. Build with this repository as overlay:

```
# poudriere bulk -f /path/to/portslist -j myjail -p default -O drrbports
```

Enjoy !
